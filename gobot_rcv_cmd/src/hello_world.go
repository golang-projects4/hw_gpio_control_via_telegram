package main

import (
	"fmt"

	hwCtrl "gobot_rcvr_cmd/src/hw_ctrl"

	"github.com/Squirrel-Network/gobotapi"
	"github.com/Squirrel-Network/gobotapi/types"
)

var gpioDevPName string = "/dev/gpio_ctrl.0"

func main() {
	var gpio hwCtrl.GpioDev
	err := gpio.OpenDev(gpioDevPName)
	if nil != err {
		panic(err)
	}
	defer gpio.CloseDev()

	client := gobotapi.NewClient("5577562006:AAFLGFvOzBLuQeidq2MBqNfKvFtd9ZZl-3w")
	// Add listener to receive only stop commands with the default alias
	client.OnCommand("command1", nil, func(client *gobotapi.Client, update types.Message) {
		fmt.Println("rcv cmd 1")
		gpio.SetGpioState(0, hwCtrl.HiBitState)
	})
	client.OnCommand("command2", nil, func(client *gobotapi.Client, update types.Message) {
		fmt.Println("rcv cmd 2")
		gpio.SetGpioState(0, hwCtrl.LoBitState)
	})
	// Start and idle the bot
	err = client.Run()
	panic(err)
}
