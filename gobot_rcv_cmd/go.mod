module gobot_rcvr_cmd

go 1.18

require (
	github.com/Squirrel-Network/gobotapi v1.2.6
	github.com/rizzza/smart v0.0.0-20190505152634-909a45200d6d
)

require golang.org/x/sys v0.0.0-20220614162138-6c1b26c55098 // indirect
