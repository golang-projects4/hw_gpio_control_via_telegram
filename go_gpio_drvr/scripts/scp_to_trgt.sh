#! /bin/bash

THIS_SCR=$(dirname $0)
IP_ADDR="192.168.1.8"
TARGET_DIR="/root/bolshevikov/go_gpio_driver"

ssh root@$IP_ADDR "if [[ ! -d $TARGET_DIR ]]; then mkdir -p $TARGET_DIR; fi"
scp -r $THIS_SCR/../bin/* root@$IP_ADDR:$TARGET_DIR
