package main

import (
	hwCtrl "gpioCtrl/src/hw_ctrl"
	"time"
)

var gpioDevPName string = "/dev/gpio_ctrl.0"

func main() {
	var gpio hwCtrl.GpioDev
	err := gpio.OpenDev(gpioDevPName)
	if nil != err {
		panic(err)
	}
	defer gpio.CloseDev()

	for i := 0; i < 5; i++ {
		gpio.SetGpioState(0, hwCtrl.HiBitState)
		time.Sleep(time.Millisecond * 500)
		gpio.SetGpioState(0, hwCtrl.LoBitState)
		time.Sleep(time.Millisecond * 500)
	}
}
