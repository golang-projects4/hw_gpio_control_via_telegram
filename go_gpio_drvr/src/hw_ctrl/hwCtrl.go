package hwCtrl

import (
	"errors"
	"syscall"
	"unsafe"

	"github.com/rizzza/smart/ioctl"
)

/*
#include <stdlib.h>

#include <linux/types.h>
#ifndef u32
#   define u32 __u32
#endif
#ifndef u64
#   define u64 __u64
#endif

size_t get_u32_size()
{
	return sizeof(u32);
}

*/
import "C"

// Структура дескриптора устройства GPIO
type GpioDev struct {
	// дескриптор открытого устройства драйвера gpio
	fd int
	// размер регистра требуемый для чтения ИЗ драйвера
	regSize int
	// указатель на выделенную память для возврата значений из драйвера
	ioctlReadBuffer unsafe.Pointer
}

// тип состояний отдельных битов gpio
type BitState int

// код активного состояния вывода gpio
const HiBitState BitState = 1

// код состояния выключенного вывода gpio
const LoBitState BitState = 0

// magic value для ioctl команды (должно соотв. magic'у в драйвере)
const ioctlMagic uintptr = 'G'
const ioctlSetDirNum uintptr = 3
const ioctlSetDataNum uintptr = 5

var (
	// код ioctl команды записи в регистр данных
	ioctlSetDataCMD uintptr
	// код ioctl команды установки направления работы gpio
	ioctlSetDirCMD uintptr
)

// инициализатор пакета
func init() {
	ioctlSetDataCMD = ioctl.Iow(
		ioctlMagic,
		ioctlSetDataNum,
		uintptr(C.get_u32_size()),
	)
	ioctlSetDirCMD = ioctl.Iow(
		ioctlMagic,
		ioctlSetDirNum,
		uintptr(C.get_u32_size()),
	)
}

// Функция открытия устройства драйвера. После окончания работы должна быть
// вызвана парная функция закрытия устройства
func (dev *GpioDev) OpenDev(devPName string) error {
	fd, err := syscall.Open(devPName, syscall.O_RDWR, 0666)
	if nil != err {
		return err
	}

	dev.regSize = int(C.get_u32_size())
	dev.ioctlReadBuffer = C.malloc(C.size_t(dev.regSize))
	if dev.ioctlReadBuffer == nil {
		syscall.Close(fd)
		return errors.New("malloc error")
	}

	dev.fd = fd
	return err
}

// Функция закрывающяя устройство драйвера
func (dev *GpioDev) CloseDev() {

	C.free(unsafe.Pointer(dev.ioctlReadBuffer))
	syscall.Close(dev.fd)
}

func (dev *GpioDev) SetGpioState(bitNum uint, state BitState) error {

	if bitNum > 31 {
		return errors.New("Слишком большой номер бита")
	}

	var initVal uint32
	if HiBitState == state {
		initVal = 1
	} else {
		initVal = 0
	}

	var data uint32 = initVal << bitNum

	err := ioctl.Ioctl(
		uintptr(dev.fd),
		ioctlSetDataCMD,
		uintptr(data),
	)
	return err
}
