function! UpdateCscopeDb()
    cscope kill 0
    echo system("mk_cscope_list -c")
    cscope add ./cscope.files
    echo system("cscope -bq")
    :cs add ./cscope.out
    :cs add ./cscope_kernel.out
endfunction


autocmd VimEnter * :cs add ./cscope_kernel.out

