/*****************************************************************************
 * Проект:
 *      "gpio_ctrl"
 *
 * Описание:
 *      Макроопределения кодов ioctl команд модулю ядра "gpio_ctrl".
 *
 * Комментарии:
 *
 * Разработчик:
 *      Igor Bolshevikov <bolshevikov.igor@gmail.com>
 *
 * license:
 *      This program is free software; you can redistribute it and/or modify it
 *      under the terms of the GNU General Public License as published by the
 *      Free Software Foundation; either version 2 of the License, or (at your
 *      option) any later version.
 *
 ****************************************************************************/
#ifndef _GPIO_CTRL_SHARE_WITH_US_H_
#define _GPIO_CTRL_SHARE_WITH_US_H_

#include <linux/types.h>
#ifndef u32
#   define u32 __u32
#endif
#ifndef u64
#   define u64 __u64
#endif
/******************************************************************************
 * Типы
 *****************************************************************************/
typedef struct 
{
    size_t  one;
    size_t  two;
    size_t  three;
} regs_set_t;

/******************************************************************************
 * Макроопределения кодов ioctl команд
 *****************************************************************************/
/** магическое чиисло - идентификатор ioctl команды */
#define GPIO_LKM_IOC_MAGIC          ('G')

/**
 * Получить тестовую структуру из драйвера
 */
#define GPIO_GET_TEST_DATA          (_IOR(GPIO_LKM_IOC_MAGIC, 1, void*))
/**
 * Записать тестовую структуру в драйвер
 */
#define GPIO_SET_TEST_DATA          (_IOW(GPIO_LKM_IOC_MAGIC, 2, void*))


/**
 * Записать регистр управления направлением IO порта
 */
#define GPIO_SET_DIR                (_IOW(GPIO_LKM_IOC_MAGIC, 3, u32))
/**
 * Прочитать регистр управления направлением IO порта
 */
#define GPIO_GET_DIR                (_IOR(GPIO_LKM_IOC_MAGIC, 4, u32*))


/**
 * Записать регистр данных (установить состояние в IO работающих на передачу)
 */
#define GPIO_SET_DATA               (_IOW(GPIO_LKM_IOC_MAGIC, 5, u32))
/**
 * Прочитать регистр данных (опрос IO линий работающих на прием)
 */
#define GPIO_GET_DATA               (_IOR(GPIO_LKM_IOC_MAGIC, 6, u32*))

/** максимальное количество ioctl команд */
#define GPIO_LKM_IOC_MAXNR          (6)


#endif /* _GPIO_CTRL_SHARE_WITH_US_H_ */
