#! /bin/bash

CONT_NAME="xilinx_buildroot_sdk:bsau_sk.instr"

THIS_PATH=$(dirname $(realpath $0))
THIS_SCR=$(basename $0)
PRJ_DIR=$(realpath $THIS_PATH/../project)

if [[ ! -d $PRJ_DIR ]]; then
    echo "$THIS_SCR: директория $PRJ_DIR не найдена. Выхожу"
    exit 1
fi

docker run                  \
    -ti                     \
    --rm                    \
    -v $PRJ_DIR:$PRJ_DIR    \
    -w $PRJ_DIR             \
    $CONT_NAME              \
    "$@"

exit $?
