#! /bin/bash

THIS_PATH=$(dirname $(realpath $0))
THIS_SCR=$(basename $0)
RUN_SCRIPT="$THIS_PATH/.run_container.sh"

$RUN_SCRIPT /bin/bash -c "          \
    source /home/sdk-user/path.conf \
 && make clean_all                  \
 "
